#include <bits/stdc++.h>
#include "lib/Site.h"
#include "lib/Tangle.h"
#include "lib/txselect.h"


#define MAX_TIME 2000000

using namespace std;


int main(int argc, char ** argv) {
	double lambda;
	int h;
	ofstream logcsv;
	bool tangleExport = false;
	int lite = 0;
	ofstream tanglecsv;

	if (argc < 4) {
		cerr << "Usage : popov <lambda> <h> <filename> [lite generation] [tangle export filename]" << "\n";
		return 128;
	} else {
		try {
			lambda = stof(argv[1]);
			h = stoi(argv[2]);
		} catch (invalid_argument const &e) {
			cerr << "Bad agruments\n";
			return 1;
		}
	}

	logcsv.open(argv[3]);
	if (logcsv.is_open()) {
		logcsv << "t;L(t)\n";
	} else {
		cerr << "Couldn't open " << argv[2] << "\n";
		return 10;
	}

	// for a faster calculation
	if (argc >= 5) {
		try {
			lite = stoi(argv[4]);
			if (lite) cerr << "\033[33mWARNING : lite generation (no computation of score & cum. wgt)\033[00m\n";
		} catch (invalid_argument const &e) {
			cerr << "Bad arguments\n";
			return 1;
		}
	}

	// CSV file to export the raw tangle
	if (argc >= 6) {
		tangleExport = true;
		tanglecsv.open(argv[5]);
		if (tanglecsv.is_open()) {
			tanglecsv << "time;id;parent1;parent2\n";
		}
	}


	srand(time(NULL));

	default_random_engine generator(time(NULL));
	poisson_distribution<int> distribution(lambda);


	Tangle t0 = Tangle();

	pair<Site, Site> genesis = t0.init(0);

	list<Site*> exportList;

	if (tangleExport) {
		exportList.push_back(&genesis.first);
		exportList.push_back(&genesis.second);
	}
	/*
	For each time unit, the random generator is used to determine if a new site is to be created and appended to the Tangle.
	*/

	cout << "Generating Tangle... \n";
	for (int time = h; time < MAX_TIME; time++) {
		int toCreate = distribution(generator);
		for (int i = 0; i < toCreate; i++) {
			pair <Site*, Site*> selectedTips = randomUniform(t0, time - h);
			Site *t;
			if (lite) t = t0.addSite(selectedTips.first, selectedTips.second, time, true);
			else t = t0.addSite(selectedTips.first, selectedTips.second, time);
			if (tangleExport) exportList.push_back(t);
		}

		if (time % 10000 == 0) cerr << "Current time : " << time << "\n";
	}
	// extracting the tips over time. 
	cout << "Extracting L0\n";
	for (int t=0; t <= MAX_TIME; t++) {
		logcsv << t << ";" << t0.getTipsAtTime(t).size() << "\n";
	}

	cout << "Successfuly written " << argv[3] << "\n";
	logcsv.close();

	if (tangleExport) {
		cout << "exporting Tangle...\n";
		for (auto s = exportList.begin(); s != exportList.end(); s++) {
			Site *exporting = *s;
			tanglecsv
				<< exporting->getTime() << ";"
				<< exporting << ";"
				<< exporting->getParent1() << ";"
				<< exporting->getParent2() << "\n";
		}
		cout << "Successfully exported thte Tangle to " << argv[5] << " \n";

		tanglecsv.close();
	}


	return 0;
}
