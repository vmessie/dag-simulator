
build-debug:
	g++ -Wall -g -c lib/Site.cpp -o bin/Site.o
	g++ -Wall -g -c lib/Tangle.cpp -o bin/Tangle.o
	g++ -Wall -g -c lib/txselect.cpp -o bin/txselect.o
	g++ -Wall -g -c main.cpp -o bin/main.o
	g++ -Wall -g -o bin/dagtest bin/*.o

clean:
	rm bin/*.o


popov:
	g++ -Wall -O3 -g -c lib/Site.cpp -o bin/Site.o
	g++ -Wall -O3 -g -c lib/Tangle.cpp -o bin/Tangle.o
	g++ -Wall -O3 -g -c lib/txselect.cpp -o bin/txselect.o
	g++ -Wall -O3 -g -c popovtangle.cpp -o bin/popovtangle.o
	g++ -Wall -O3 -g -o bin/popov bin/*.o
