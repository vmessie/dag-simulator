#include <bits/stdc++.h>
#include "robin_hood.h"

using namespace std;

// Global tip selection methods

pair <Site*, Site*> randomUniform(Tangle t, int time);
pair <Site*, Site*> randomUniform(Tangle t, int time1, int time2);
Site *chooseOneAtRandom(robin_hood::unordered_set <Site*> toSelect);
pair <Site*, Site*> chooseAtRandom(robin_hood::unordered_set <Site*> toSelect);

pair <Site*, Site*> MCMCAtGenesis(Tangle t, int time);
pair <Site*, Site*> MCMCAtDepth(Tangle t, int time, int depth);
