#include <bits/stdc++.h>
#include "robin_hood.h"
//#include <list.h>
#include "Site.h"
#include "Tangle.h"
#include "txselect.h"

// "Random uniform" method
pair <Site*, Site*> randomUniform(Tangle t, int time) {
	robin_hood::unordered_set <Site*> toSelect = t.getTipsAtTime(time);
	return chooseAtRandom(toSelect);
}

// "Random Uniform" but the tips are chosen at two different times?
// The function is implemented so that duplicates are avoided
pair <Site*, Site*> randomUniform(Tangle t, int time1, int time2) {
	robin_hood::unordered_set <Site*> toSelect1 = t.getTipsAtTime(time1);
	robin_hood::unordered_set <Site*> toSelect2 = t.getTipsAtTime(time2);

	Site *selected1 = NULL;
	Site *selected2 = NULL;

	if (toSelect1.size() == 1) selected1 = *toSelect1.begin();
	else selected1 = chooseOneAtRandom(toSelect1);

	if (toSelect2.size() == 1) selected2 = *toSelect2.begin();
	else selected2 = chooseOneAtRandom(toSelect2);

	if (selected1 == selected2) {
		if (toSelect1.size() == 1 && toSelect2.size() == 1) return make_pair(selected1, selected1->getParent1());
		else if (toSelect1.size() == 1)
			do {
				selected2 = chooseOneAtRandom(toSelect2);
			} while (selected2 == selected1);

		else
			do {
				selected1 = chooseOneAtRandom(toSelect1);
			} while (selected1 == selected2);
	}


	return make_pair(selected1, selected2);
}

// Selects a random tip into a set
Site *chooseOneAtRandom(robin_hood::unordered_set <Site*> toSelect) {
	auto r = rand() % toSelect.size();
	auto it = toSelect.begin();
	advance(it, r);

	return *it;
}

// Selects a pair of two distinct sites in a set.
pair <Site*, Site*> chooseAtRandom(robin_hood::unordered_set <Site*> toSelect) {
	Site *t1 = NULL;
	Site *t2 = NULL;

	auto r = rand() % toSelect.size();
	auto it = toSelect.begin();
	advance(it, r);

	t1 = *it;

	if (toSelect.size() == 1) t2 = t1->getParent1();
	else do {
		auto r = rand() % toSelect.size();
		auto it = toSelect.begin();
		advance(it, r);
		t2 = *it;
	} while (t1 == t2);

	return make_pair(t1, t2);
}

// NOT YET IMPLEMENTED
pair <Site*, Site*> MCMC(Tangle t, int time) {
	Site *t1 = NULL;
	Site *t2 = NULL;
	return make_pair(t1, t2);
}
