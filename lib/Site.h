#include <bits/stdc++.h>

using namespace std;

// To represent a Site of the Tangle
class Site {
	private:
		Site *parent1;
		Site *parent2;
		Site *lastConfirming;
		unordered_set <Site*> childs;
		int time;
		int confirmations;
		int score;
		int calc_score();
		string group;

	public:
		Site(int time);
		Site(Site *p1, Site *p2, int time);
		Site(Site *p1, Site *p2, int time, string group);
		Site(Site *p1, Site *p2, int time, bool lite);
		int isTip();
		int getScore();
		int getCumWGT();
		int getTime();
		string getGroup();
		Site *getParent1();
		Site *getParent2();
		unordered_set <Site*> getChilds();

		void addChild(Site *child);
		void addConfirmation(Site *confirming);

		unordered_set<Site*> getAncestor();

		void populateAncestors(unordered_set<Site*> *ancestors);
};
