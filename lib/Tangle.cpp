#include <bits/stdc++.h>

#include "Site.h"
#include "Tangle.h"
#include "robin_hood.h"

using namespace std;

// A third party set library is used instead of the standard one, for the sake of efficiency.
Tangle::Tangle() {
	this->tips = new robin_hood::unordered_set <Site*> ();
}

Tangle::Tangle(Tangle *t) {
	this->tips = new robin_hood::unordered_set <Site*> (t->getLastTips());
	this->lastTime = t->lastTime;
	this->tipsHistory = t->tipsHistory;
}

Tangle::Tangle(Site *genA, Site *genB) {
	int timeOfCreation = genA->getTime();
	this->tips = new robin_hood::unordered_set <Site*> ();
	this->lastTime = genB->getTime();
	this->tips->insert(genA);
	this->tips->insert(genB);
	tipsHistory[timeOfCreation] = *(tips);
}

// Initialisation function. Returns two tips as the genesis of the Tangle
pair <Site, Site> Tangle::init(int timeOfCreation) {
	pair <Site, Site> genesisSites = make_pair(Site(timeOfCreation), Site(timeOfCreation +1));

	this->tips->insert(&(genesisSites.first));
	this->tips->insert(&(genesisSites.second));

	tipsHistory[timeOfCreation] = *(tips);
	return genesisSites;
}
robin_hood::unordered_set <Site*> Tangle::getLastTips() {
	return *(this->tips);
}
// A history of the tips of the Tangle is saved, so we are able to get the tips that existed at a given time
robin_hood::unordered_set <Site*> Tangle::getTipsAtTime(int timestamp) {
	map<int, robin_hood::unordered_set<Site*>>::iterator it;

	if (timestamp >= this->tipsHistory.rbegin()->first) return this->getLastTips();
	for (it = this->tipsHistory.begin(); it != this->tipsHistory.end(); it++) {
		if (it->first > timestamp) break;
	}

	it--;

	return it->second;
}

Site* Tangle::addSite(Site* t1, Site* t2, int timestamp) {
	Site* newSite = new Site(t1, t2, timestamp);
	this->tips->insert(newSite);
	this->tips->erase(t1);
	this->tips->erase(t2);

	tipsHistory[timestamp] = *(tips);


	return newSite;
}

Site* Tangle::addSite(Site* t1, Site* t2, int timestamp, string group) {
	Site* newSite = new Site(t1, t2, timestamp, group);

	this->tips->insert(newSite);
	this->tips->erase(t1);
	this->tips->erase(t2);

	tipsHistory[timestamp] = *(tips);

	return newSite;
}

Site* Tangle::addSite(Site* t1, Site* t2, int timestamp, bool lite) {


	Site* newSite = new Site(t1, t2, timestamp, lite);


	this->tips->insert(newSite);
	this->tips->erase(t1);
	this->tips->erase(t2);

	tipsHistory[timestamp] = *(tips);


	return newSite;
}

// This method is only called if two overlapping Tangle instances are generated
void Tangle::distantCall(int timestamp, Site *p1, Site *p2, Site *n){

	tipsHistory[timestamp] = *(tips);
}

map <int, robin_hood::unordered_set<Site*>> Tangle::getSiteHistory() {
	return this->tipsHistory;
}
