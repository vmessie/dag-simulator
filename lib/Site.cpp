#include "Site.h"
#include <bits/stdc++.h>

using namespace std;

// default constructor
Site::Site(int time) {
	this->time = time;
	this->parent1 = NULL;
	this->parent2 = NULL;
	this->confirmations = 0;
	this->lastConfirming = NULL;

	this->group = "DEFAULT";
}

Site::Site(Site *p1, Site *p2, int time) {
	this->time = time;
	this->parent1 = p1;
	this->parent2 = p2;
	this->confirmations = 0;
	this->lastConfirming = NULL;

	if (p1 != NULL) p1->addChild(this);
	if (p2 != NULL) p2->addChild(this);

	this->group = "DEFAULT";

}

Site::Site(Site *p1, Site *p2, int time, string group) {
	this->time = time;
	this->parent1 = p1;
	this->parent2 = p2;
	this->confirmations = 0;
	this->lastConfirming = NULL;

	if (p1 != NULL) p1->addChild(this);
	if (p2 != NULL) p2->addChild(this);

	this->group = group;
}

// If "lite" option is used, cumulative wgt. and score are not being calculated
Site::Site(Site *p1, Site *p2, int time, bool lite) {
	this->time = time;
	this->parent1 = p1;
	this->parent2 = p2;
	this->confirmations = 0;
	this->lastConfirming = NULL;
}


int Site::isTip() {
	return childs.size() == 0;
}

int Site::getScore() {
	return this->getAncestor().size() - 1;
}

int Site::getCumWGT() {
	return this->confirmations;
}

int Site::getTime() {
	return this->time;
}

string Site::getGroup() {
	return this->group;
}

Site *Site::getParent1() {
	return this->parent1;
}

Site *Site::getParent2() {
	return this->parent2;
}

unordered_set <Site*> Site::getChilds() {
	return this->childs;
}

void Site::addChild(Site *child) {
	this->childs.insert(child);
	addConfirmation(child);
}

/* A recursive method used to calculate the cumulative weight of the Site.
If a new site is appended to the Tangle and there exists a direct reversed path to it, then this method will be triggered, and the cumulative weight incremented
*/
void Site::addConfirmation(Site *confirming) {
	if (confirming == this->lastConfirming) return;
	this->lastConfirming = confirming;
	this->confirmations++;
	if (this->parent1 != NULL) this->parent1->addConfirmation(confirming);
	if (this->parent2 != NULL) this->parent2->addConfirmation(confirming);
}

unordered_set<Site*> Site::getAncestor() {
	unordered_set<Site*> *ancestors = new unordered_set<Site*>;
	this->populateAncestors(ancestors);
	return *ancestors;
}
// A recursive method to get all the ancestors of the Site
void Site::populateAncestors(unordered_set<Site*> *ancestors) {
	cout << this << "\n";
	ancestors->insert(this);
	if (this->parent1 != NULL && ancestors->find(this->parent1) == ancestors->end())
		this->parent1->populateAncestors(ancestors);
	if (this->parent2 != NULL && ancestors->find(this->parent2) == ancestors->end())
	    this->parent2->populateAncestors(ancestors);
}
