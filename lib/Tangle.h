#include <bits/stdc++.h>
#include "robin_hood.h"

using namespace std;

// This class represents an instance of a Tangle
class Tangle {
	private:
		robin_hood::unordered_set<Site*> *tips;
	protected:
		int lastTime;
		map <int, robin_hood::unordered_set<Site*>> tipsHistory;
	public:
		Tangle();
		Tangle(Site *gen1, Site *gen2);
		Tangle(Tangle *t);
		pair <Site, Site> init(int timeOfCreation);
		robin_hood::unordered_set <Site*> getLastTips();
		robin_hood::unordered_set <Site*> getTipsAtTime(int timestamp);
		Site* addSite(Site* t1, Site* t2, int timestamp);
		Site* addSite(Site* t1, Site* t2, int timestamp, string group);
		Site* addSite(Site* t1, Site* t2, int timestamp, bool lite);
		void distantCall(int timestamp, Site *p1, Site *p2, Site *n);
		map <int, robin_hood::unordered_set<Site*>> getSiteHistory();
};
