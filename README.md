# Dag Simulator

This DAG simulator generates a mock tangle, and extract various parameters such as the evolution of the count of tips over time into csv files.
The entry parameters are the transaction throughput (simulated by a poisson point process) and a (constant) latency.

`tips.csv` is provided as an example CSV file, generated with parameters `lambda = 0.01617; h = 100000`

## Dependencies
Standard C++ libraries
R environment for visualisation

## Compilation

    make clean
	make popov

## Usage
    popov <lambda> <h> <filename> [lite generation] [tangle export filename]

	Then the `displaytips.R` script may be used to plot the count of tips over time.

### required parameters
* `lambda` transaction throughput
* `h` transaction latency
* `filename` : path to CSV file to export the count of tips over time

### optional parameters
* `lite generation` : set to one for a quicker, yet simpler tangle generation (no pre-calculation of score and cum. weight)
* `tangle export filename` : if set, the whole tangle will be exported in a CSV file at the provided path
